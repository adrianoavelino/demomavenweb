package br.uninove.demomavenweb.controller;

public class ClienteController {
	public String salvar() {	
		return "list";
	}
	public String cancelar() {	
		System.out.println("Salvando ...");
		return "list";
	}	
	
	public String adicionar() {	
		System.out.println("Adicionando ...");
		return "edit";
	}

	public String excluir() {	
		System.out.println("Excluindo...");
		return null;
	}
	
	public String editar() {	
		System.out.println("Editando...");
		return "edit";
	}	
	
}
